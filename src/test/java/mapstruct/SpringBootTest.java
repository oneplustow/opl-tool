package mapstruct;

import cn.opl.mapstruct.DefaultMapStructAdapter;
import cn.opl.spring.boot.autoconfigure.MapStructAutoConfiguration;
import cn.opl.mapstruct.MapStructContext;
import org.junit.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * @author CC
 * @title: SpringBootTest
 * @projectName opl-tool
 * @description:
 * @date 2021/6/1521:35
 */

public class SpringBootTest {

    @Test
    public void test(){
        AnnotationConfigApplicationContext annotationConfigApplicationContext = new AnnotationConfigApplicationContext();
        //annotationConfigApplicationContext.register(DefaultMapStructAdapter.class);
        annotationConfigApplicationContext.register(UserMapStructImpl.class);
        annotationConfigApplicationContext.register(MapStructAutoConfiguration.class);
        annotationConfigApplicationContext.refresh();
        System.out.println(annotationConfigApplicationContext.getBean(MapStructContext.class));
    }
}
