package mapstruct;

import cn.opl.mapstruct.DefaultMapStructAdapter;
import cn.opl.mapstruct.MapStructContext;
import org.junit.Test;

/**
 * @author CC
 * @title: MapStructContextTest
 * @projectName opl-tool
 * @description:
 * @date 2021/6/1422:28
 */
public class MapStructContextTest {

    @Test
    public void mapStructContextTest(){
        MapStructContext mapStructContext = new MapStructContext(new DefaultMapStructAdapter());
        mapStructContext.registerMapStructs(new IntensifyUserMapStructImpl());
        mapStructContext.initContext();
        User user = new User();
        user.setUserName("cc");
        user.setAge(24);
        UserDto userDto = mapStructContext.conver(user, UserDto.class);
        System.out.println(userDto);


    }
}
