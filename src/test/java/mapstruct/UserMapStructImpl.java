package mapstruct;

import cn.hutool.core.convert.Converter;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author CC
 * @title: UserMapStructImpl
 * @projectName opl-tool
 * @description:
 * @date 2021/6/1422:27
 */
@Component
public class UserMapStructImpl extends User implements Converter<User>,UserMapStruct {
    @Override
    public UserDto converFrom(User user) {
        UserDto userDto = new UserDto();
        userDto.setUserName(user.getUserName());
        userDto.setAge(user.getAge());
        return userDto;
    }

    @Override
    public User converTo(UserDto userDto) {
        return null;
    }

    @Override
    public List<User> converTo(List<UserDto> userDtos) {
        return null;
    }

    @Override
    public List<UserDto> converFrom(List<User> users) {
        return null;
    }

    @Override
    public User convert(Object value, User defaultValue) throws IllegalArgumentException {
        return null;
    }
}
