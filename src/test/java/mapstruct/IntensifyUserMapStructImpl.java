package mapstruct;

/**
 * @author CC
 * @title: IntensifyUserMapStructImpl
 * @projectName opl-tool
 * @description:
 * @date 2021/6/1423:07
 */
public class IntensifyUserMapStructImpl extends UserMapStructImpl{
    @Override
    public UserDto converFrom(User user) {
        UserDto userDto = super.converFrom(user);
        userDto.setSex("男");
        return userDto;
    }
}
