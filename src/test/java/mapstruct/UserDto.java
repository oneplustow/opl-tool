package mapstruct;

import lombok.Data;

/**
 * @author CC
 * @title: User
 * @projectName opl-tool
 * @description:
 * @date 2021/6/1422:24
 */
@Data
public class UserDto {
    private String userName;
    private Integer age;
    private String sex;
}
