package mapstruct;

import cn.opl.mapstruct.IMapStruct;
import cn.opl.mapstruct.IMapStructAdapter;
import org.mapstruct.Mapper;

/**
 * @author CC
 * @title: UserMapStruct
 * @projectName opl-tool
 * @description:
 * @date 2021/6/1422:25
 */
@Mapper
public interface UserMapStruct extends IMapStruct<User,UserDto> {
}
