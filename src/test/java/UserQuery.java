import cn.opl.annotation.Query;
import cn.opl.enums.QueryType;
import lombok.Data;

import java.util.List;

@Data
public class UserQuery {

    @Query(QueryType.EQ)
    private String userName;

    @Query(QueryType.GT)
    private Integer age;

    @Query(value = QueryType.EQ,or = true)
    private String sex;

    @Query(value = QueryType.EQ,or = true)
    private String idcar;

    @Query(QueryType.LIKE)
    private String address;

    @Query(QueryType.IN)
    private List<String> phoneList;
}
