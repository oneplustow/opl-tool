package cn.opl.annotation;

import cn.opl.enums.QueryType;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 查询注解
 * @author cc
 * @date  2021年6月11日
 */
@Target({ElementType.METHOD, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface Query {
    QueryType value() default QueryType.EQ;

    String fieldName() default "";

    boolean or() default false;


}
