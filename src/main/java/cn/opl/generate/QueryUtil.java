package cn.opl.generate;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.ReflectUtil;
import cn.hutool.core.util.StrUtil;
import cn.opl.annotation.Query;
import cn.opl.enums.QueryType;
import cn.opl.generate.handler.QueryGenerateWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * @author cc
 * @date 2021年6月11日
 */
public class QueryUtil {

    public static <T> QueryWrapper<T> generate(Object obj){
        //orList 用来存放所有进行 or 连接的条件查询
        List<QueryGenerateWrapper> orList = new ArrayList<>();
        //获取所有字段
        Field[] fields = ReflectUtil.getFields(obj.getClass());
        QueryWrapper<T> queryWrapper = new QueryWrapper();
        for (Field field : fields) {
            //判断字段上是否有Query注解
            Query annotation = field.getAnnotation(Query.class);
            if(annotation == null){continue;}
            QueryType type = annotation.value();

            Object value = null;
            try {
                //获取字段值
                value = ReflectUtil.getFieldValue(obj,field);
            }catch (Exception e){
                throw new RuntimeException(e);
            }

            if(value == null){continue;}

            //获取fieldName名称，如果规定了fieldName 则使用，否则取字段的名称转下划线，当查询字段
            String fieldName = annotation.fieldName();
            if (StrUtil.isBlank(fieldName)) {
                fieldName = StrUtil.toUnderlineCase(field.getName());
            }
            QueryGenerateWrapper queryGenerateWrapper = new QueryGenerateWrapper(queryWrapper,fieldName,value, type);
            if (annotation.or()) {
                orList.add(queryGenerateWrapper);
            }
            queryTypeHandler(queryGenerateWrapper);
        }
        if(CollUtil.isNotEmpty(orList)) {
            queryWrapper.and(wrapper -> {
                for (QueryGenerateWrapper queryGenerateWrapper : orList) {
                    queryGenerateWrapper.setQueryWrapper(wrapper);
                    queryTypeHandler(queryGenerateWrapper);
                    wrapper.or();
                }
            });
        }

        return queryWrapper;
    }

    private static void queryTypeHandler(QueryGenerateWrapper queryGenerateWrapper) {
        QueryWrapper queryWrapper = queryGenerateWrapper.getQueryWrapper();
        QueryType type = queryGenerateWrapper.getType();
        switch (type) {
            case EQ:queryWrapper.eq(queryGenerateWrapper.getFieldName(),queryGenerateWrapper.getValue());
                break;
            case NOT_EQ:queryWrapper.ne(queryGenerateWrapper.getFieldName(),queryGenerateWrapper.getValue());
                break;
            case GT:queryWrapper.gt(queryGenerateWrapper.getFieldName(),queryGenerateWrapper.getValue());
                break;
            case IN:
                Collection inValue = parseCollection(queryGenerateWrapper.getValue());
                queryWrapper.in(queryGenerateWrapper.getFieldName(),inValue);
                break;
            case NOT_IN:
                Collection notInValue = parseCollection(queryGenerateWrapper.getValue());
                queryWrapper.notIn(queryGenerateWrapper.getFieldName(),notInValue);
                break;
            case LT:queryWrapper.lt(queryGenerateWrapper.getFieldName(),queryGenerateWrapper.getValue());
                break;
            case GT_EQ:queryWrapper.ge(queryGenerateWrapper.getFieldName(),queryGenerateWrapper.getValue());
                break;
            case LT_EQ:queryWrapper.le(queryGenerateWrapper.getFieldName(),queryGenerateWrapper.getValue());
                break;
            case LIKE:
                queryWrapper.like(queryGenerateWrapper.getFieldName(), queryGenerateWrapper.getValue());
                break;
            case LIKE_LEFT:queryWrapper.likeLeft(queryGenerateWrapper.getFieldName(),queryGenerateWrapper.getValue());
                break;
            case LIKE_RIGHT:queryWrapper.likeRight(queryGenerateWrapper.getFieldName(),queryGenerateWrapper.getValue());
                break;
            default:
        }
    }

    private static Collection parseCollection(Object value) {
        if(value instanceof Collection){
            return (Collection)value;
        }
        if(value instanceof String){
            return StrUtil.split((String)value,',');
        }
        return null;
    }

}
