package cn.opl.generate.handler;

import cn.opl.enums.QueryType;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author cc
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class QueryGenerateWrapper {

    private QueryWrapper queryWrapper;

    private String fieldName;

    private Object value;

    private QueryType type;

    public QueryGenerateWrapper(String fieldName, Object value, QueryType type) {
        this.fieldName = fieldName;
        this.value = value;
        this.type = type;
    }
}
