package cn.opl.spring.boot.autoconfigure;

import cn.opl.mapstruct.DefaultMapStructAdapter;
import cn.opl.mapstruct.IMapStructAdapter;
import cn.opl.mapstruct.MapStructContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;

import javax.sql.DataSource;


/**
 * @author CC
 * @title: AutoConfigMapStruct
 * @projectName opl-tool
 * @description:
 * @date 2021/6/1521:23
 */

@ConditionalOnMissingBean(MapStructContext.class)
@Configurable
public class MapStructAutoConfiguration {


    @Bean
    public MapStructContext mapStructContext(ApplicationContext context,IMapStructAdapter mapStructAdapter){
        MapStructContext mapStructContext = new MapStructContext(mapStructAdapter);
        String[] beanNamesForType = context.getBeanNamesForType(mapStructContext.getiMapStructClass());
        for (String beanName : beanNamesForType) {
            mapStructContext.registerMapStructs(context.getBean(beanName));
        }
        mapStructContext.initContext();
        return mapStructContext;
    }

    @Bean
    @ConditionalOnMissingBean({IMapStructAdapter.class})
    public IMapStructAdapter mapStructAdapter(){
        return new DefaultMapStructAdapter();
    }
}
