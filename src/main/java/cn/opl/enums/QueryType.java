package cn.opl.enums;

public enum QueryType {
    /**等于*/
    EQ,
    /**不等于*/
    NOT_EQ,
    /**大于*/
    GT,
    /**大于等于*/
    GT_EQ,
    /**小于*/
    LT,
    /**小于等于*/
    LT_EQ,
    /**中 like */
    LIKE,
    /** 右 like */
    LIKE_RIGHT,
    /** 左 like */
    LIKE_LEFT,
    /** 包含 in 查询*/
    IN,
    /**不包含 in 查询*/
    NOT_IN,

}
