package cn.opl.mapstruct;

import java.util.List;

/**
 * 适配器对象，用来进行MapStruct的适配
 * @author CC
 * @title: IMapStructAdapter
 * @projectName opl-tool
 * @description:
 * @date 2021/6/1421:11
 */
public interface IMapStructAdapter<T>{
    /**
     * 将 from对象转成 to 对象
     * @param t
     * @param from
     * @return
     */
    public Object converFrom(T t,Object from);

    /**
     * 将 to 对象转成 from 对象
     * @param t
     * @param from
     * @return
     */
    public Object converTo(T t,Object to) ;

    /**
     * 将 to 对象集合转成 from 对象集合
     * @param t
     * @param toList
     * @return
     */
    public List converTo(T t,List toList);

    /**
     * 将 from 对象集合转成 to 对象集合
     * @param t
     * @param toList
     * @return
     */
    public List converFrom(T t,List fromList);
}
