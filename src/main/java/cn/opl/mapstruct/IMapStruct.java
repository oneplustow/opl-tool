package cn.opl.mapstruct;

import java.util.List;

/**
 * @author cc
 */
public interface IMapStruct<F,T> {

    /**
     * 将对象 F（From） 转换到对象 T（To）
     * @param f 需要转换的对象
     * @return 转换后的对象
     */
    T converFrom(F f);

    /**
     * 将对象 T（To） 转换到对象 S（From）
     * @param t 需要转换的对象
     * @return 转换后的对象
     */
    F converTo(T t);

    /**
     * 将对象 tList（To） 转换到对象 sList（From）
     * @param tList 需要转换的对象
     * @return 转换后的对象
     */
    List<F> converTo(List<T> tList);

    /**
     * 将对象 sList（From）转换到对象 tList（To）
     * @param fList 需要转换的对象
     * @return 转换后的对象
     */
    List<T> converFrom(List<F> fList);
}
