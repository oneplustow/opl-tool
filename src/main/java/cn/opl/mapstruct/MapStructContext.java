package cn.opl.mapstruct;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.lang.Assert;
import lombok.Data;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 实体类转换器上下文
 * 对所有实体类转换器 StructMapper 进行汇总解析
 * 对外提供统一方法进行调用
 *
 * @author cc
 */
public class MapStructContext {

    private List<Object> mapStructs = new ArrayList<>();

    private Class iMapStructClass;

    private IMapStructAdapter iMapStructAdapter;

    public Class getiMapStructClass() {
        return iMapStructClass;
    }

    public IMapStructAdapter getiMapStructAdapter() {
        return iMapStructAdapter;
    }

    public MapStructContext(IMapStructAdapter iMapStructAdapter) {
        this.iMapStructAdapter = iMapStructAdapter;
        ReflectUtil.genericityMap(iMapStructAdapter,IMapStructAdapter.class,(type, adapter)->iMapStructClass = (Class) type[0]);
    }

    public void registerMapStructs(List<Object> mapStructs) {
        this.mapStructs.addAll(mapStructs);
    }
    public void registerMapStructs(Object mapStruct) {
        this.mapStructs.add(mapStruct);
    }

    /**
     * 实体映射转换器缓存
     * key为 fromClass +:+ toClass
     * 例如：com.String:com.Integer
     * value为 StructMapperParse解析对象
     */
    private ConcurrentHashMap<String, MapStructParse> map = new ConcurrentHashMap<>(48);

    public <T> List<T> conver(List<?> from, Class<T> to) {
        if (CollUtil.isEmpty(from)) {
            return new ArrayList<T>();
        }
        Boolean forward = isForward(from.get(0).getClass(), to);

        Assert.notNull(forward, "无法找到具体的转换规则");

        Object iMapStruct = getIMapStruct(from.get(0).getClass(), to);

        Assert.notNull(iMapStruct, "无法找到具体的转换规则");

        return forward ? iMapStructAdapter.converFrom(iMapStruct,from) : iMapStructAdapter.converTo(iMapStruct,from);
    }

    public <T> T conver(Object from, Class<T> to) {
        if (from == null) {
            return null;
        }
        Boolean forward = isForward(from.getClass(), to);
        Assert.notNull(forward, "无法找到具体的转换规则");
        Object iMapStruct = getIMapStruct(from.getClass(), to);
        Assert.notNull(iMapStruct, "无法找到具体的转换规则");
        return forward ? (T) iMapStructAdapter.converFrom(iMapStruct,from) : (T) iMapStructAdapter.converTo(iMapStruct,from);
    }

    /**
     * 通过当前class和 转换到的class  来获取是正向查询还是反向查询
     * 正向查询则表示 和 接口泛型一致的转换换 A.class  -> B.class
     * 反向查询则表 和接口泛型相反的转换 B.class  -> A.class
     * 为空则表示 没有转换实体
     *
     * @param from 转换对象
     * @param to 转换后的对象
     * @return true 正向查询，false 反向查询，空没有转换
     */
    public Boolean isForward(Class from, Class to) {
        String fromTypeName = from.getTypeName();
        String toTypeName = to.getTypeName();
        //正向查询
        String key = fromTypeName + ":" + toTypeName;
        if (map.containsKey(key)) {
            return true;
        }
        //反向查询
        key = toTypeName + ":" + fromTypeName;
        if (map.containsKey(key)) {
            return false;
        }
        return null;
    }

    /**
     * 通过当前class和 转换到的class  来获取entityMapper 实体转换映射器
     * 获取有两种情况，一种是正向查询，一种是反向查询
     * 正向查询则表示 和 接口泛型一致的转换换 A.class  -> B.class
     * 反向查询则表示 和接口泛型相反的转换 B.class  -> A.class
     *
     * @param from 转换对象
     * @param to 转换后的对象
     * @return 转换实体对象 空没有转换
     */
    public Object getIMapStruct(Class from, Class to) {
        String fromTypeName = from.getTypeName();
        String toTypeName = to.getTypeName();
        //正向查询
        String key = fromTypeName + ":" + toTypeName;
        if (map.containsKey(key)) {
            return map.get(key).getIMapStruct();
        }
        //反向查询
        key = toTypeName + ":" + fromTypeName;
        if (map.containsKey(key)) {
            return map.get(key).getIMapStruct();
        }
        return null;
    }

    /**
     * 在初始化完成后执行
     * 对ico里面所有的entityMapper进解析，
     * 将解析后的数据保存到map映射表
     * 来表明 A.class ->  B.class
     */
    public void initContext() {
        if (CollUtil.isEmpty(mapStructs)) {
            return;
        }
        for (Object iMapStruct : mapStructs) {
            ReflectUtil.genericityMap(iMapStruct,iMapStructClass,(type, o)->{
                MapStructParse structMapperParse = new MapStructParse(type[0], type[1], o);
                map.put(structMapperParse.getKey(), structMapperParse);
            });
        }
    }


    @Data
    class MapStructParse {
        private String from;
        private String to;
        private Type fromType;
        private Type toType;
        private Object iMapStruct;

        public String getKey() {
            return from + ":" + to;
        }

        public MapStructParse(Type fromType, Type toType, Object iMapStruct) {
            this.from = fromType.getTypeName();
            this.to = toType.getTypeName();
            this.fromType = fromType;
            this.toType = toType;
            this.iMapStruct = iMapStruct;
        }

    }

}
