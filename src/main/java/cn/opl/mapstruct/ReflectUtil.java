package cn.opl.mapstruct;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.map.multi.ListValueMap;
import cn.hutool.core.util.ArrayUtil;
import sun.reflect.generics.reflectiveObjects.ParameterizedTypeImpl;

import java.lang.reflect.Type;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.BiConsumer;

/**
 * @author cc
 */
public class ReflectUtil extends cn.hutool.core.util.ReflectUtil {

    /**
     * 解析data集合，获取data里面实现clazz的泛型类型，以泛型为key data为value的Map进行返回
     * @param data 需要解析的集合，实现了clazz
     * @param clazz 具有泛型的接口
     * @param <R> 返回类型
     * @return 泛型为key，data为value的Map进行返回
     */
    public static <R> Map<Type, R> genericityMap(List<R> data, Class<R> clazz){
        Map<Type,R> typeRmap = new HashMap<>(16);

        genericityMap(data,clazz,(type, r)->typeRmap.put(type[0],r));
        return typeRmap;
    }

    /**
     * 解析data集合，获取data里面实现clazz的泛型类型，以泛型为key List&lt;data&gt;为value的Map进行返回
     * <br/>
     * 适合同一种泛型会存在多个对象的情况
     * @param data 需要解析的集合，实现了clazz
     * @param clazz 具有泛型的接口
     * @param <R> 返回类型
     * @return 泛型为key List&lt;data&gt;为value的Map进行返回
     */
    public static <R> Map<Type, Collection<R>> genericityListMap(List<R> data, Class<R> clazz){
        ListValueMap<Type,R> typeRmap = new ListValueMap();
        genericityMap(data,clazz,(type, r)->typeRmap.putValue(type[0],r));
        return typeRmap.getRaw();
    }

    /**
     * 解析data集合，获取data里面实现clazz的泛型类型，以泛型1为key,Map&lt;泛型2,data&gt; 为value的Map进行返回
     * <br/>
     * 适合有两个泛型的情况
     * @param data 需要解析的集合，实现了clazz
     * @param clazz 具有泛型的接口
     * @param <R> 返回类型
     * @return 以泛型1为key,Map&lt;泛型2,data&gt; 为value的Map进行返回
     */
    public static <R> Map<Type, Map<Type,R>> genericityBygenericityMap(List<R> data, Class<R> clazz){
        Map<Type, Map<Type,R>> typeMapMap = new HashMap<>(16);
        genericityMap(data,clazz,(type, r)-> {
            Type key1 = type[0],key2 = type[1];
            Map<Type, R> typeMap = typeMapMap.computeIfAbsent(key1, k -> new HashMap<>(16));
            typeMap.put(key2,r);
        });
        return typeMapMap;
    }


    /**
     * 解析data集合，获取data里面实现clazz的泛型类型，以泛型数组为第一个参数，data为第二个参数，进行consumer的回调
     * @param data 需要解析的集合，实现了clazz
     * @param clazz 具有泛型的接口
     * @param consumer 函数接口，进行回调
     * @param <R> 没具体含义
     */
    public static <R> void genericityMap(List<R> data, Class<R> clazz, BiConsumer<Type[],R> consumer){
        if (CollUtil.isEmpty(data)) {
            return;
        }
        for (R r : data) {
            genericityMap(r, clazz, consumer);
        }
    }

    /**
     * 解析r对象，获取 r 里面实现clazz的泛型类型，以泛型数组为第一个参数，r为第二个参数，进行consumer的回调
     * @param r 需要解析的对象，实现了clazz
     * @param clazz 具有泛型的接口
     * @param consumer 函数接口，进行回调
     * @param <R> 没具体含义
     */
    public static <R> void genericityMap(R r, Class<R> clazz, BiConsumer<Type[], R> consumer) {
        ParameterizedTypeImpl parameterizedType = getParameterizedType(r.getClass(), clazz);
        if (parameterizedType == null) {
            return;
        }
        Type[] actualTypeArguments = parameterizedType.getActualTypeArguments();
        consumer.accept(actualTypeArguments, r);
    }

    private static <R> ParameterizedTypeImpl getParameterizedType(Type type, Class<R> clazz){
        if(type instanceof Class){
            if (!clazz.isAssignableFrom((Class)type)) {return null;}
            return parseClass((Class)type,clazz);
        }
        if(type instanceof ParameterizedTypeImpl){
            ParameterizedTypeImpl parameterizedType = (ParameterizedTypeImpl) type;
            if (!clazz.isAssignableFrom((parameterizedType).getRawType())) {return null;}
            return parseParameterizedType(parameterizedType,clazz);
        }
        return null;
    }

    private static ParameterizedTypeImpl parseParameterizedType(ParameterizedTypeImpl parameterizedType, Class clazz) {
        return parameterizedType;
    }

    private static ParameterizedTypeImpl parseClass(Class anInterface,Class clazz) {
        //获取当前类的所有接口
        Type[] genericInterfaces = anInterface.getGenericInterfaces();
        Type genericSuperclass = anInterface.getGenericSuperclass();
        Type[] genericTypes = ArrayUtil.append(genericInterfaces, genericSuperclass);

        for (Type genericType : genericTypes) {
            ParameterizedTypeImpl parameterizedType = getParameterizedType(genericType, clazz);
            if(parameterizedType != null){return parameterizedType;}
        }
        return null;
    }

}
