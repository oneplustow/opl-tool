package cn.opl.mapstruct;

import java.util.List;

/**
 * @author CC
 * @title: DefaultMapStructAdapter
 * @projectName opl-tool
 * @description:
 * @date 2021/6/1421:22
 */
public class DefaultMapStructAdapter implements IMapStructAdapter<IMapStruct>{

    @Override
    public Object converFrom(IMapStruct iMapStruct, Object o) {
        return iMapStruct.converFrom(o);
    }

    @Override
    public Object converTo(IMapStruct iMapStruct, Object o) {
        return iMapStruct.converTo(o);
    }

    @Override
    public List converTo(IMapStruct iMapStruct, List list) {
        return iMapStruct.converTo(list);
    }

    @Override
    public List converFrom(IMapStruct iMapStruct, List list) {
        return iMapStruct.converFrom(list);
    }

}
